import matplotlib.pyplot as plt
import numpy as np


def fun(x, y):
    return np.exp(-(x**2 + y**2)) + 0.8 * np.exp(-3*((x-1)**2 + (y-1)**2))

def fun2(x, y):
    return 1.2 + 0.02*x**2 + 0.05*y**2

plt.style.use("dark_background")
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = y = np.arange(-3.0, 3.0, 0.05)
X, Y = np.meshgrid(x, y)

zs = np.array([fun(x,y) for x,y in zip(np.ravel(X), np.ravel(Y))])
Z = zs.reshape(X.shape)
ax.plot_surface(X, Y, -Z, label=r'$\mathcal{L}$', color='blue', alpha=0.5)

# zs = np.array([fun2(x,y) for x,y in zip(np.ravel(X), np.ravel(Y))])
# Z = zs.reshape(X.shape)
# ax.plot_surface(X, Y, Z, label=r'$\mathcal{D}$', color='cyan')

x = np.linspace(-3,0)
y = [0 for i in x]
z = -fun(x, 0)
plt.plot(x, y, z, color='red',linewidth=7.0)

n = 40
point = np.array([x[n], y[n], z[n]])
R = np.linalg.norm(point)

r = 0.6
u, v = np.mgrid[0:2 * np.pi:30j, 0:np.pi:20j]
x = r * (np.cos(u) * np.sin(v)) - 0.3
y = r * (np.sin(u) * np.sin(v))
z = r * (np.cos(v) / 4.5) - 0.9
ax.plot_surface(x, y, z)



# plt.legend(loc='best', bbox_to_anchor=(0.3, 0.5))
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.set_zticklabels([])
plt.show()
